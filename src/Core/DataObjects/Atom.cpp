#include "Atom.h"

Atom::Atom()
{

}

Atom::~Atom()
{

}

std::string Atom::name() const
{
    return m_name;
}

void Atom::setName(const std::string &name)
{
    m_name = name;
}
unsigned int Atom::index() const
{
    return m_index;
}

void Atom::setIndex(unsigned int index)
{
    m_index = index;
}

float Atom::rad() const
{
    return m_rad;
}

void Atom::setRad(float rad)
{
    m_rad = rad;
}

std::string Atom::elementName() const
{
    return m_elementName;
}

void Atom::setElementName(const std::string &elementName)
{
    m_elementName = elementName;
    // set radii and std-color based on element name

    // convert pico to nano
    this->setRad(AtomLUT::vdW_radii_picometer.find(m_elementName)->second/1000.0);

    AtomLUT::colorMap::iterator it = AtomLUT::cpk_colorcode.find(m_elementName);
    if(it != AtomLUT::cpk_colorcode.end())
        this->setColor(it->second);
    else
        this->setColor(AtomLUT::cpk_colorcode.find("other")->second);
}

std::string Atom::residueName() const
{
    return m_residueName;
}

void Atom::setResidueName(const std::string &residueName)
{
    m_residueName = residueName;
}

int Atom::numBounds() const
{
    return m_numBounds;
}

void Atom::setNumBounds(int numBounds)
{
    m_numBounds = numBounds;
}

AtomLUT::color Atom::color() const
{
    return m_color;
}

void Atom::setColor(const AtomLUT::color &color)
{
    m_color = color;
}



