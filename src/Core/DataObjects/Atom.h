#ifndef ATOM_H
#define ATOM_H

#include <string>
#include <glm/vec3.hpp>
#include "AtomLUT.h"

/**
 * @brief The Atom class
 * This class contains the parameters of a single atom.
 */
class Atom{

public:

    Atom();
    ~Atom();

    std::string name() const;
    void setName(const std::string &name);

    unsigned int index() const;
    void setIndex(unsigned int index);

    float rad() const;
    void setRad(float rad);

    std::string elementName() const;
    void setElementName(const std::string &elementName);

    std::string residueName() const;
    void setResidueName(const std::string &residueName);

    int numBounds() const;
    void setNumBounds(int numBounds);

    AtomLUT::color color() const;
    void setColor(const AtomLUT::color &color);

private:
    std::string m_name;
    std::string m_elementName;
    std::string m_residueName;
    int m_numBounds;
    unsigned int m_index;
    float m_rad;
    AtomLUT::color m_color;
};
#endif // ATOM_H
