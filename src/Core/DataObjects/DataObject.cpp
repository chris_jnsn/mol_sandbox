#include "DataObject.h"

DataObject::DataObject(std::string id)
    : m_isVisible(true)
{
    m_ID = id;
}

std::string DataObject::ID() const
{
    return m_ID;
}

void DataObject::setID(const std::string &ID)
{
    m_ID = ID;
}

bool DataObject::isVisible() const
{
    return m_isVisible;
}

void DataObject::setIsVisible(bool isVisible)
{
    m_isVisible = isVisible;
}

DataObject::~DataObject()
{

}
