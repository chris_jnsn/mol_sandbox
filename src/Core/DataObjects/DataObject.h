#ifndef DATAOBJECT_H
#define DATAOBJECT_H

#include <iostream>

class DataObject{

public:

    DataObject(std::string id);

    std::string ID() const;
    void setID(const std::string &ID);

    bool isVisible() const;
    void setIsVisible(bool isVisible);

    virtual ~DataObject();

private:
    std::string m_ID;
    bool m_isVisible;
};

#endif //DATAOBJECT_H
