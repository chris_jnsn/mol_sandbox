#include "Frame.h"


Frame::Frame()
{

}

Frame::~Frame()
{

}

unsigned int Frame::index() const
{
    return m_frameIndex;
}

void Frame::setIndex(unsigned int index)
{
    m_frameIndex = index;
}
std::vector<Frame::atom_position> Frame::atomPositions() const
{
    return m_atomPositions;
}

void Frame::setAtomPositions(const std::vector<atom_position> &atomPositions)
{
    m_atomPositions = atomPositions;
}

int Frame::numberOfAtoms()
{
    return m_atomPositions.size();
}


