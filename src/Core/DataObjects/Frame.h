#ifndef FRAME_H
#define FRAME_H

#include <string>
#include <vector>
#include <glm/vec3.hpp>


/**
 * @brief The Frame class
 * This class contains the atoms of single trajectory frame.
 */
class Frame{



public:

    Frame();
    ~Frame();

    struct atom_position{
        glm::vec3 position;
        int atom_index;
    };

    unsigned int index() const;
    void setIndex(unsigned int index);

    std::vector<atom_position> atomPositions() const;
    void setAtomPositions(const std::vector<atom_position> &atomPositions);

    int numberOfAtoms();

private:

    unsigned int m_frameIndex;
    std::vector<atom_position> m_atomPositions;
};


#endif // FRAME_H
