#include "TimeResolvedDataObject.h"

TimeResolvedDataObject::TimeResolvedDataObject(std::string id)
    :DataObject(id)
    ,m_currentFrame(0)
{

}

int TimeResolvedDataObject::currentFrame() const
{
    return m_currentFrame;
}

void TimeResolvedDataObject::setCurrentFrame(int currentFrame)
{
    m_currentFrame = currentFrame;
}

int TimeResolvedDataObject::minFrame() const
{
    return m_minFrame;
}

int TimeResolvedDataObject::maxFrame() const
{
    return m_maxFrame;
}

TimeResolvedDataObject::~TimeResolvedDataObject()
{

}


