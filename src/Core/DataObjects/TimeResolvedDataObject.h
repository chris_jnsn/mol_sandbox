#ifndef TIMERESOLVEDDATAOBJECT_H
#define TIMERESOLVEDDATAOBJECT_H

#include "DataObject.h"

class TimeResolvedDataObject : public DataObject
{
public:
    TimeResolvedDataObject(std::string id);
    int currentFrame() const;
    void setCurrentFrame(int currentFrame);

    int minFrame() const;

    int maxFrame() const;

    virtual ~TimeResolvedDataObject();

protected:
    int m_currentFrame;
    int m_minFrame;
    int m_maxFrame;
};

#endif //TIMERESOLVEDDATAOBJECT_H
