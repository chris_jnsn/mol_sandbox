#include "Topology.h"


std::string Topology::name() const
{
    return m_name;
}

void Topology::setName(const std::string &name)
{
    m_name = name;
}
std::vector<Atom> Topology::getAtoms() const
{
    return m_atoms;
}

void Topology::setAtoms(const std::vector<Atom> &value)
{
    m_atoms = value;
}

void Topology::addAtom(Atom atom)
{
    m_atoms.push_back(atom);
}

Atom Topology::getAtom(int index)
{
    if (index >= m_atoms.size())
    {
        std::printf("Atom index out of bounds!");
    }
    return m_atoms.at(index);
}



Topology::~Topology()
{

}


Topology::Topology()
{

}

Topology::Topology(int numAtoms)
{
    for (int i = 0; i < numAtoms; i++)
    {
        Atom a;
        m_atoms.push_back(a);
    }
}
