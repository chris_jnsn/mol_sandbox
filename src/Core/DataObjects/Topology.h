#ifndef TOPOLOGY_H
#define TOPOLOGY_H

#include <string>
#include <vector>
#include "Atom.h"

/**
 * @brief The Topology class
 * This class describes the topology of a molecule and its atoms.
 */
class Topology{

public:

    Topology();
    Topology(int numAtoms);
    ~Topology();

    std::string name() const;
    void setName(const std::string &name);

    std::vector<Atom> getAtoms() const;
    void setAtoms(const std::vector<Atom> &value);

    void addAtom(Atom atom);

    Atom getAtom(int index);

    // Bounds

private:

    std::string m_name;
    std::vector<Atom> m_atoms;
};


#endif // TOPOLOGY_H
