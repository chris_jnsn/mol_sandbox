#include "Trajectory.h"

Trajectory::Trajectory()
{

}

Trajectory::~Trajectory()
{

}

std::vector<Frame> Trajectory::frames() const
{
    return m_frames;
}

void Trajectory::setFrames(const std::vector<Frame> &frames)
{
    m_frames = frames;
    m_numFrames = m_frames.size();
}
std::string Trajectory::name() const
{
    return m_name;
}

void Trajectory::setName(const std::string &name)
{
    m_name = name;
}

int Trajectory::numFrames() const
{
    return m_numFrames;
}


