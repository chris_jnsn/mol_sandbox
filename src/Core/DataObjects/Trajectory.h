#ifndef TRAJECTORY_H
#define TRAJECTORY_H

#include <string>
#include <vector>
#include "Frame.h"

/**
 * @brief The Trajectory class
 * This class holds the trajectory frames of a molecue simulation.
 */
class Trajectory{

public:

    Trajectory();
    ~Trajectory();

    std::vector<Frame> frames() const;
    void setFrames(const std::vector<Frame> &frames);

    std::string name() const;
    void setName(const std::string &name);

    int numFrames() const;

private:

    std::string m_name;
    std::vector<Frame> m_frames;
    int m_numFrames;
};


#endif // TRAJECTORY_H
