#include "DynamicMolecule.h"

DynamicMolecule::DynamicMolecule(std::string id)
    :TimeResolvedDataObject(id)
{

}

DynamicMolecule::~DynamicMolecule()
{

}
Trajectory DynamicMolecule::trajectory() const
{
    return m_trajectory;
}

void DynamicMolecule::setTrajectory(const Trajectory &trajectory)
{
    m_trajectory = trajectory;
    m_minFrame = 0;
    m_maxFrame = trajectory.numFrames()-1;
}
Topology DynamicMolecule::topology() const
{
    return m_topology;
}

void DynamicMolecule::setTopology(const Topology &topology)
{
    m_topology = topology;
}




