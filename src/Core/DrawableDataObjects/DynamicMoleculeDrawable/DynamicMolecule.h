#ifndef DYNAMICMOLECULE_H
#define DYNAMICMOLECULE_H

#include "Meta/Drawable.h"
#include "DataObjects/TimeResolvedDataObject.h"
#include "DataObjects/Trajectory.h"
#include "DataObjects/Topology.h"

/**
 * @brief The DynamicMolecule class
 * This class contains molecule trajectories and the topology of the molecule.
 * It is drawable via the visito pattern.
 */

class DynamicMolecule:  public TimeResolvedDataObject, public Drawable{

public:
    DynamicMolecule(std::string id);
    ~DynamicMolecule();

    Trajectory trajectory() const;
    void setTrajectory(const Trajectory &trajectory);

    Topology topology() const;
    void setTopology(const Topology &topology);

private:
    Trajectory m_trajectory;
    Topology m_topology;
};


#endif // DYNAMICMOLECULE_H
