#include "DataManager.h"

DataNode* DataManager::newDataNode(std::string name)
{
    DataNode *node = new DataNode(name);
    std::pair<std::map<std::string, DataNode*>::iterator,bool> ret;
    m_DataNodesMap.insert(std::pair<std::string, DataNode*>(name, node));
    if (ret.second == false)
        std::cerr << "DataNode with name: " << name << " already exists" << std::endl;
    this->setActiveDataNode(node);
    return node;
}

DataNode *DataManager::getDataNode(std::string name)
{
    return m_DataNodesMap.at(name);
}

DataNode *DataManager::getDataNode()
{
    return this->getDataNode("default");
}

DataNode *DataManager::getActiveDataNode() const
{
    return m_activeDataNode;
}

void DataManager::registerListener(DataManagerListener *listener)
{
    m_listeners.push_back(listener);
}

DataObject *DataManager::getActiveDataObject() const
{
    return m_activeDataObject;
}

void DataManager::updateActiveDataObjectRequest(DataNode *node)
{
    if (node == m_activeDataNode)
        this->setActiveDataObject(m_activeDataNode->getActiveDataObject());
}

DataManager::DataManager()
{
    // add a default datanode
    this->newDataNode("default");
}

void DataManager::setActiveDataNode(DataNode *node)
{
    m_activeDataNode = node;
    this->activeDataNodeChanged();
    this->setActiveDataObject(m_activeDataNode->getActiveDataObject());
}

void DataManager::setActiveDataObject(DataObject *dataObject)
{
    m_activeDataObject = dataObject;
    this->activeDataObjectChanged();
}

void DataManager::activeDataNodeChanged()
{
    for (DataManagerListener* l : m_listeners)
    {
        l->activeDataNodeChanged();
    }
}

void DataManager::activeDataObjectChanged()
{
    for (DataManagerListener* l : m_listeners)
    {
        l->activeDataObjectChanged();
    }
}

