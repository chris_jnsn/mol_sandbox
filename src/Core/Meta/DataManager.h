#ifndef DATAMANGER_H
#define DATAMANGER_H

#include <map>
#include <vector>
#include "DataNode.h"
#include "DataObjects/DataObject.h"
#include "Meta/DataManagerListener.h"

class DataManager
{
public:
    static DataManager& getInstance()
    {
        static DataManager    instance; // Guaranteed to be destroyed.
        // Instantiated on first use.
        return instance;
    }

    DataNode *newDataNode(std::string name);
    DataNode* getDataNode(std::string name);
    DataNode* getDataNode();

    DataNode *getActiveDataNode() const;

    void registerListener(DataManagerListener* listener);

    DataObject *getActiveDataObject() const;

    // node tells the datamanager that its active data object has changed
    void updateActiveDataObjectRequest(DataNode* node);

private:
    DataManager();
    DataManager(DataManager const&) = delete;
    void operator=(DataManager const&) = delete;

    std::map<std::string, DataNode*> m_DataNodesMap;

    DataNode* m_activeDataNode;
    DataObject* m_activeDataObject;

    std::vector<DataManagerListener*> m_listeners;

    void setActiveDataNode(DataNode* node);
    void setActiveDataObject(DataObject* dataObject);
    void activeDataNodeChanged();
    void activeDataObjectChanged();

};

#endif // DATAMANGER_H
