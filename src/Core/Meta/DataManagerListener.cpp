#include "DataManagerListener.h"
#include "Meta/DataManager.h"

DataManagerListener::DataManagerListener()
{
    DataManager::getInstance().registerListener(this);
}
