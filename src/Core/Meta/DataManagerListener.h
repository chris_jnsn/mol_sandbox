#ifndef DATAMANAGERLISTENER_H
#define DATAMANAGERLISTENER_H

//class DataManager;

class DataManagerListener
{

public:
    DataManagerListener();
    virtual void activeDataObjectChanged() = 0;
    virtual void activeDataNodeChanged() = 0;

    virtual ~DataManagerListener() {};
};

#endif //DATAMANAGERLISTENER_H
