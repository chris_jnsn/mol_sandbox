#include "DataNode.h"
#include "DataManager.h"

DataNode *DataNode::newChildDataNode(std::string name)
{
    DataNode *node = new DataNode(name);
    std::pair<std::map<std::string, DataNode*>::iterator,bool> ret;
    m_DataNodesMap.insert(std::pair<std::string, DataNode*>(name, node));
    if (ret.second == false)
        std::cerr << "DataNode with name: " << name << " already exists" << std::endl;
    return node;
}

DataNode *DataNode::getDataNode(std::string name)
{
    return m_DataNodesMap.at(name);
}

void DataNode::addDataObject(DataObject *dataObject)
{
    std::pair<std::map<std::string, DataObject*>::iterator,bool> ret;
    ret = m_DataObjectsMap.insert(std::pair<std::string, DataObject*>(dataObject->ID(), dataObject));
    if (ret.second == false)
        std::cerr << "DataObject with ID: " << dataObject->ID() << " already exists" << std::endl;
    m_activeDataObject = dataObject;

    // TODO
    // connect dataobject with proper renderer if not set yet

    // tell the DataManager that the active object of this node has changed
    DataManager::getInstance().updateActiveDataObjectRequest(this);
}

DataObject *DataNode::getDataObject(std::string name)
{
    return m_DataObjectsMap.at(name);
}

DataNode::DataNode(std::string name)
{
    m_name = name;
    m_activeDataObject = 0;
}

DataObject *DataNode::getActiveDataObject() const
{
    return m_activeDataObject;
}

