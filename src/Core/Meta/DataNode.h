#ifndef DATANODE_H
#define DATANODE_H

#include <iostream>
#include <map>
#include "DataObjects/DataObject.h"

//class DataManager;

class DataNode
{
public:

    DataNode* newChildDataNode(std::string name);
    DataNode* getDataNode(std::string name);

    void addDataObject(DataObject *dataObject);
    DataObject *getDataObject(std::string name);

    inline bool operator==(DataNode &d)
    {
        return m_name.compare(d.m_name) == 0 ? true : false;
    }

private:
    friend class DataManager;
    DataNode(std::string name);
    std::string m_name;
    std::map<std::string, DataNode*> m_DataNodesMap;
    std::map<std::string, DataObject*> m_DataObjectsMap;

    DataObject *getActiveDataObject() const;
    DataObject* m_activeDataObject;
};

#endif //DATANODE_H
