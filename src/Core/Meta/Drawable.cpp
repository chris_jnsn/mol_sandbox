#include "Drawable.h"
#include "Renderer.h"

Drawable::Drawable()
{

}

Drawable::~Drawable()
{

}

void Drawable::setRenderer(Renderer* renderer)
{
    m_renderer = renderer;
}

void Drawable::initializeRenderer()
{
    m_renderer->initialize(this);
    m_renderer->setRendererInitialized(true);
}

Renderer *Drawable::renderer()
{
    return m_renderer;
}

void Drawable::draw()
{
    m_renderer->prepareAndRender();
}



