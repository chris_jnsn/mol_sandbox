#ifndef DRAWABLE_H
#define DRAWABLE_H

class Renderer;
class OpenGLWindow;
class OpenGLWidget;

class Drawable
{
public:
    Drawable();
    virtual ~Drawable();

    Renderer* renderer();
    void draw();
    void initializeRenderer();

private:

    friend class ::OpenGLWindow;
    friend class ::OpenGLWidget;
    Renderer* m_renderer;
    void setRenderer(Renderer *renderer);
};

#endif //DRAWABLE_H
