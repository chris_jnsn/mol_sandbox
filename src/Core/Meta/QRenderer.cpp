#include "QRenderer.h"
#include "qgltogl.h"

void QRenderer::prepareAndRender()
{
    if (!m_openglInitialized)
    {
        initializeOpenGLFunctions();
//        m_renderWidget->context()->setNativeHandle(contextHandle);
//        m_renderWidget->context()->create();
        gl45func = m_renderWidget->context()->versionFunctions<QOpenGLFunctions_4_5_Core>();//m_renderWindow->m_context->versionFunctions<QOpenGLFunctions_4_3_Core>();
        if (gl45func)
          gl45func->initializeOpenGLFunctions();
        m_openglInitialized = true;
    }
    renderSafe();
}

GLuint QRenderer::loadShader(GLenum type, const char *source)
{
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, 0);
    glCompileShader(shader);
    return shader;
}

QRenderer::QRenderer()
{

}

QRenderer::~QRenderer()
{

}

OpenGLWidget *QRenderer::renderWidget() const
{
    return m_renderWidget;
}

void QRenderer::setRenderWidget(OpenGLWidget *renderWidget)
{
    m_renderWidget = renderWidget;
    m_isConsumed = true; // renderer cannot be used for other windows
}

