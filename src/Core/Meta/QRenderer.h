#ifndef QRENDERER_H
#define QRENDERER_H

#include <QOpenGLFunctions>
#include "Drawable.h"
#include "QOpenGL/QOpenGLWidget/OpenGLWidget.h"
#include <qopenglfunctions_4_5_core.h>
#include "Renderer.h"

class QRenderer: protected QOpenGLFunctions, public Renderer
{    
public:
    QRenderer();
    ~QRenderer();

    void prepareAndRender();

    GLuint loadShader(GLenum type, const char *source);

protected:

    friend class OpenGLWidget;
    OpenGLWidget *renderWidget() const;
    void setRenderWidget(OpenGLWidget *renderWidget);
    OpenGLWidget *m_renderWidget;
    QOpenGLFunctions_4_5_Core* gl45func; // the name has to match the call object used in qgltogl.h
};

#endif //QRENDERER_H
