#include "Renderer.h"


void Renderer::initialize(Drawable *drawable)
{
    if(!m_rendererInitialized)
        m_rendererInitialized = initializeOverride(drawable);
}

bool Renderer::initializeOverride(Drawable *drawable)
{

}

void Renderer::prepareAndRender()
{

}

void Renderer::render(){

}

GLuint Renderer::loadShader(GLenum type, const char *source)
{
}

Renderer::Renderer()
    : m_openglInitialized(false)
    , m_rendererInitialized(false)
    , m_isConsumed(false)
{

}

Renderer::~Renderer()
{

}

bool Renderer::rendererInitialized() const
{
    return m_rendererInitialized;
}

void Renderer::setRendererInitialized(bool rendererInitialized)
{
    m_rendererInitialized = rendererInitialized;
}

void Renderer::renderSafe()
{
    if(m_rendererInitialized)
        render();
}

bool Renderer::isConsumed() const
{
    return m_isConsumed;
}

Camera *Renderer::camera() const
{
    return m_camera;
}

void Renderer::setCamera(Camera *camera)
{
    m_camera = camera;
}

