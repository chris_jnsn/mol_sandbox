#ifndef RENDERER_H
#define RENDERER_H

#include "Drawable.h"
#include <GLFW/glfw3.h>
#include "SceneManager/Camera.h"

class Renderer
{    
public:

    void initialize(Drawable* drawable);

    /**
     * @brief initializeOverride
     * @param drawable
     * @return True if initialization was successful.
     */
    virtual bool initializeOverride(Drawable* drawable);
    virtual void render();

    virtual void prepareAndRender();

    virtual GLuint loadShader(GLenum type, const char *source);

    Renderer();
    ~Renderer();

    Camera *camera() const;
    void setCamera(Camera *camera);

    bool isConsumed() const;

protected:
    friend class Drawable;
    Drawable* m_drawable;

    bool rendererInitialized() const;
    void setRendererInitialized(bool rendererInitialized);

    bool m_openglInitialized;
    bool m_rendererInitialized;

    Camera* m_camera; // the camera is set by the window that this renderer draws into

    bool m_isConsumed; // the renderer instance can only be attached to a single window and drawable

private:
    friend class QRenderer;
    void renderSafe();    

};

#endif //RENDERER_H
