#include "MainOpenGLWidget.h"
#include "Renderer/DynamicMoleculeRenderer.h"
#include "Renderer/DynamicMoleculeRenderer_DetectVisible.h"
#include "Renderer/ExampleRenderer.h"
#include "Meta/DataManager.h"

#include <QMouseEvent>
#include <QKeyEvent>


MainOpenGLWidget::MainOpenGLWidget(OpenGLWidget *parent)
    : OpenGLWidget(parent)
{

}

MainOpenGLWidget::~MainOpenGLWidget(){}

void MainOpenGLWidget::activeDataObjectChanged()
{
    // TODO
    // check if the active data object has a renderer attached
    // if not, attach an appropriate renderer

    DynamicMoleculeRenderer_DetectVisible* renderer = new DynamicMoleculeRenderer_DetectVisible;
    //ExampleRenderer* renderer = new ExampleRenderer;
    this->addDrawable(dynamic_cast<Drawable*>(DataManager::getInstance().getActiveDataObject()), renderer);
}

void MainOpenGLWidget::activeDataNodeChanged()
{

}

void MainOpenGLWidget::mouseMoveEvent(QMouseEvent *e)
{
    int x = e->x(); int y = e->y();
    m_camera->Move2D(x,y);
}

void MainOpenGLWidget::mousePressEvent(QMouseEvent *e)
{
    setFocus();
    if(e->button() == Qt::LeftButton)
        m_camera->SetPos(GLFW_MOUSE_BUTTON_LEFT, GLFW_PRESS, e->x(), e->y());
}

void MainOpenGLWidget::mouseReleaseEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton)
        m_camera->SetPos(GLFW_MOUSE_BUTTON_LEFT, GLFW_RELEASE, e->x(), e->y());
}

void MainOpenGLWidget::keyPressEvent(QKeyEvent *e)
{
    char key = e->key();
    switch (key) {
    case 'W':
        m_camera->Move(FORWARD);
        break;
    case 'A':
        m_camera->Move(LEFT);
        break;
    case 'S':
        m_camera->Move(BACK);
        break;
    case 'D':
        m_camera->Move(RIGHT);
        break;
    case 'Q':
        m_camera->Move(DOWN);
        break;
    case 'E':
        m_camera->Move(UP);
        break;
    }
}
