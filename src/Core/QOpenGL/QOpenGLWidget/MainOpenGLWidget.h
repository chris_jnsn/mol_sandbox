#ifndef MAINOPENGLWINDOW_H
#define MAINOPENGLWINDOW_H

#include "OpenGLWidget.h"
#include "Meta/DataManagerListener.h"

class MainOpenGLWidget : public OpenGLWidget, public DataManagerListener
{
public:
    explicit MainOpenGLWidget(OpenGLWidget *parent = 0);
    ~MainOpenGLWidget();

    // DataManagerListener interface
public:
    void activeDataObjectChanged();
    void activeDataNodeChanged();

    // the widget is responsible to convert input into camera movement
    void mouseMoveEvent(QMouseEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);

    void keyPressEvent(QKeyEvent *e);
};

#endif//MAINOPENGLWINDOW_H
