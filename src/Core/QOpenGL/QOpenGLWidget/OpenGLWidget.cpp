#include "OpenGLWidget.h"
#include "Meta/QRenderer.h"
#include <iostream>


OpenGLWidget::OpenGLWidget(QOpenGLWidget *parent)
    : QOpenGLWidget(parent)
    , m_animating(false)
    , m_isInitialized(false)
{
    m_drawableList.clear();

    m_camera = new Camera;
    m_camera->SetMode(FREE);
    m_camera->SetPosition(glm::vec3(0, 0, -10));
    m_camera->SetLookAt(glm::vec3(0, 0, 0));
    m_camera->SetClipping(.01, 500);
    m_camera->SetFOV(45);
}

void OpenGLWidget::initializeGL()
{
    // initialize every drawables' renderer
    for(Drawable* d : m_drawableList)
    {
        d->initializeRenderer();
    }
    m_isInitialized = true;
}

void OpenGLWidget::resizeGL(int w, int h)
{

}

void OpenGLWidget::paintGL()
{
    // call draw for all drawables
    for(Drawable* d : m_drawableList)
    {
        d->draw();
    }

    if(m_animating)
        this->update();
}

bool OpenGLWidget::animating() const
{
    return m_animating;
}

void OpenGLWidget::setAnimating(bool animating)
{
    m_animating = animating;
}


int OpenGLWidget::addDrawable(Drawable *drawable, QRenderer *renderer)
{
    // check if the renderer is still available
    if (renderer->isConsumed())
    {
        std::cerr << "A renderer instance can only be used for a single window and drawable!" << std::endl;
        return -1;
    }

    drawable->setRenderer(renderer);
    renderer->setRenderWidget(this);
    renderer->setCamera(m_camera);
    m_drawableList.push_back(drawable);

    if(m_isInitialized)
        drawable->initializeRenderer();
    return m_drawableList.size() - 1;
}

OpenGLWidget::~OpenGLWidget(){}
