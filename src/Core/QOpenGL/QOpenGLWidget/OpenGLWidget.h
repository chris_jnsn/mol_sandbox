#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QOpenGLWidget>
#include "Meta/Drawable.h"
#include "SceneManager/Camera.h"
class QRenderer;

class OpenGLWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    explicit OpenGLWidget(QOpenGLWidget *parent = 0);
    ~OpenGLWidget();

    int addDrawable(Drawable* drawable, QRenderer* renderer);

    // QOpenGLWidget interface
    bool animating() const;
    void setAnimating(bool animating);

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    bool m_animating;

    std::vector<Drawable*> m_drawableList;

    Camera* m_camera; // the window owns it's own camera (or scene) and supplies it to the renderers that draw into that window

private:
    friend class Renderer;

    bool m_isInitialized;
};

#endif//OPENGLWIDGET_H
