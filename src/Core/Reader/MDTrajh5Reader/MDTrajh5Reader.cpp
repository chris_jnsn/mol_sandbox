#include "MDTrajh5Reader.h"
#include "Wrapper/MDTrajWrapper/MDTrajWrapper.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include <Python.h>
#include <numpy/arrayobject.h>
#include <iostream>


MDTrajh5Reader::MDTrajh5Reader()
{

}

MDTrajh5Reader::~MDTrajh5Reader()
{

}

DynamicMolecule* MDTrajh5Reader::loadDynamicMolecule(std::string path)
{
    wrapper = new MDTrajWrapper;


    std::string fullPath = RESOURCES_PATH;
    //prepare arguments
    if(path.substr(0,1).compare("/") != 0)
    {
        fullPath.append("/" + path);
    }
    else
        fullPath = path;

    PyObject* file = wrapper->loadFile(fullPath);
    PyObject* xyz_py = wrapper->getXYZ(file);

    ///
    /// READ XYZ POSITIONS
    ///
    // get a numpy array from the PyObject
    PyArrayObject* xyz_pyarray = reinterpret_cast<PyArrayObject*>(xyz_py);

    // get the dimonsions (number of frames, atoms) of the trajectory file
    int numFrames{ PyArray_SHAPE(xyz_pyarray)[0] };
    int numAtoms{ PyArray_SHAPE(xyz_pyarray)[1] };
    int numComponents{ PyArray_SHAPE(xyz_pyarray)[2] };
    assert(numComponents == 3);

    // cast the 3D numpy array to a 1D c array
    float* xyz_carray;
    xyz_carray = reinterpret_cast<float*>(PyArray_DATA(xyz_pyarray));

    // create a molecule and fill in the values
    DynamicMolecule* result = new DynamicMolecule(path);
    Trajectory traj;
    Frame frame;
    Frame::atom_position atom;
    std::vector<Frame> frames;
    std::vector<Frame::atom_position> atoms;
    glm::vec3 position;
    int id = 0;

    frames.clear();
    for (int f = 0; f < numFrames; f++)
    {
        atoms.clear();
        for (int a = 0; a < numAtoms; a++)
        {
            id = f * numAtoms * numComponents + a * numComponents;
            position.x = xyz_carray[id];
            position.y = xyz_carray[id+1];
            position.z = xyz_carray[id+2];
            atom.position = position;
            atom.atom_index = f * numAtoms + a;
            atoms.push_back(atom);
        }
        frame.setAtomPositions(atoms);
        frame.setIndex(f);
        frames.push_back(frame);
    }

    traj.setFrames(frames);
    result->setTrajectory(traj);

    ///
    /// END READ XYZ POSITIONS
    ///

    ///
    /// READ TOPOLOGY
    ///

    Topology top;
    PyObject* topology_py = wrapper->getTopology(file);
    std::vector<std::string> names;
    std::vector<std::string> elementNames;
    std::vector<std::string> residueNames;
    std::vector<int> indices;
    std::vector<int> numBonds;
    wrapper->getAllAtomProperties(topology_py, names, elementNames, residueNames, indices, numBonds);

    // create atoms and add them to the topology

    for (int i = 0; i < numAtoms; i++)
    {
        Atom a;
        a.setIndex(indices[i]);
        a.setName(names[i]);
        a.setElementName(elementNames[i]);
        a.setNumBounds(numBonds[i]);
        a.setResidueName(residueNames[i]);
        top.addAtom(a);
    }
    result->setTopology(top);

    ///
    /// END READ TOPOLOGY
    ///

    return result;
}

