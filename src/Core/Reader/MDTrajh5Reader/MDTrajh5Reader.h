#ifndef MDTRAJH5READER_H
#define MDTRAJH5READER_H

#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"

class MDTrajWrapper;
/**
 * @brief The MDTrajh5Reader class
 * This class reads a .h5 trajectory file via the mdtraj python module and returns a DynamicMolecule instance.
 */
class MDTrajh5Reader{
public:
    MDTrajh5Reader();
    ~MDTrajh5Reader();

    DynamicMolecule *loadDynamicMolecule(std::string path);
    MDTrajWrapper* wrapper;

};

#endif
