#ifndef DYNAMICMOLECULERENDERER_H
#define DYNAMICMOLECULERENDERER_H

#include "Meta/QRenderer.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include <QOpenGLShaderProgram>

class DynamicMoleculeRenderer: public QRenderer
{
public:
    void render() Q_DECL_OVERRIDE;
    bool initializeOverride(Drawable *drawable);
    DynamicMoleculeRenderer();

private:
    DynamicMolecule* m_dynamicMolecule;

    GLuint m_scaleAttr;
    GLuint m_viewMatrixU;
    GLuint m_projectionMatrixU;
    GLuint m_positionAttr;
    GLuint m_instancePositionAttr;
    GLuint m_colorAttr;

    QOpenGLShaderProgram *m_program;
    int m_frame;

    std::vector<std::vector<GLfloat> > m_instance_colors;
    std::vector<std::vector<GLfloat> > m_instance_positions;
    std::vector<GLfloat> m_uniformQuad;
    std::vector<int> m_numAtoms;

    void updateDataFrame();
    int m_lastFrame;
    int m_currentFrame;
};

#endif //DYNAMICMOLECULERENDERER_H
