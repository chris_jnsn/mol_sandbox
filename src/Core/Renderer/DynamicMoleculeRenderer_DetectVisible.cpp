
#include "DynamicMoleculeRenderer_DetectVisible.h"
#include "DataObjects/Frame.h"
#include <iostream>
#include <QScreen>
#include <QApplication>
#include <QOpenGLBuffer>


DynamicMoleculeRenderer_DetectVisible::DynamicMoleculeRenderer_DetectVisible()
    : m_program(0)
    , m_frame(0)
{

}

void DynamicMoleculeRenderer_DetectVisible::updateDataFrame()
{
    glVertexAttribPointer(m_positionAttr, 2, GL_FLOAT, GL_FALSE, 0, &m_uniformQuad[0]);
    glVertexAttribPointer(m_colorAttr, 4, GL_FLOAT, GL_FALSE, 0, &m_instance_colors[m_currentFrame][0]);
    glVertexAttribPointer(m_instancePositionAttr, 4, GL_FLOAT, GL_FALSE, 0, &m_instance_positions[m_currentFrame][0]);
}

bool DynamicMoleculeRenderer_DetectVisible::initializeOverride(Drawable *drawable)
{
    // get the drawable object an the values that are needed to render
    m_dynamicMolecule = dynamic_cast<DynamicMolecule*>(drawable);
    m_lastFrame = -1; // force updateFrameData once

    std::vector<Frame> frames = m_dynamicMolecule->trajectory().frames();
    int numFrames = m_dynamicMolecule->trajectory().numFrames();

    m_instance_colors.clear();
    m_instance_positions.clear();
    m_numAtoms.clear();

    std::vector<GLfloat> frameInstanceColors;
    std::vector<GLfloat> frameInstancePositions;

    Topology top = m_dynamicMolecule->topology();

    for (int f = 0; f < numFrames; f++)
    {
        frameInstanceColors.clear();
        frameInstancePositions.clear();

        Frame frame = frames[f];
        m_numAtoms.push_back(frame.numberOfAtoms());
        std::vector<Frame::atom_position> atomPositions = frame.atomPositions();

        for (int i = 0; i < m_numAtoms[f]; i++)
        {

            frameInstanceColors.push_back(top.getAtom(i).color().r);
            frameInstanceColors.push_back(top.getAtom(i).color().g);
            frameInstanceColors.push_back(top.getAtom(i).color().b);
            frameInstanceColors.push_back(0.0f);

            // positions contain xyz + size
            frameInstancePositions.push_back(atomPositions[i].position[0]);
            frameInstancePositions.push_back(atomPositions[i].position[1]);
            frameInstancePositions.push_back(atomPositions[i].position[2]);

            // no atom specific size yet, either
            //frameInstancePositions.push_back(1.7f);
            frameInstancePositions.push_back(top.getAtom(i).rad());
        }
        m_instance_colors.push_back(frameInstanceColors);
        m_instance_positions.push_back(frameInstancePositions);
    }


    m_program = new QOpenGLShaderProgram(m_renderWidget);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, SHADERS_PATH "/Impostor/impostorSpheres_Instanced.vert");
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, SHADERS_PATH "/Impostor/impostorSpheres_Instanced.frag");
    m_program->link();
    m_scaleAttr = m_program->uniformLocation("scale");
    m_viewMatrixU = m_program->uniformLocation("view");
    m_projectionMatrixU = m_program->uniformLocation("projection");
    m_colorAttr = m_program->attributeLocation("colorAttribute");
    m_positionAttr = m_program->attributeLocation("positionAttribute");
    m_instancePositionAttr = m_program->attributeLocation("instance_positionAttribute");

    m_uniformQuad.clear();
    m_uniformQuad.push_back(-1.0f);
    m_uniformQuad.push_back(-1.0f);
    m_uniformQuad.push_back(-1.0f);
    m_uniformQuad.push_back(1.0f);
    m_uniformQuad.push_back(1.0f);
    m_uniformQuad.push_back(-1.0f);
    m_uniformQuad.push_back(1.0f);
    m_uniformQuad.push_back(1.0f);


    return true;
}


void DynamicMoleculeRenderer_DetectVisible::render()
{
    m_currentFrame = m_dynamicMolecule->currentFrame();

    if(!m_openglInitialized)
        m_renderWidget->makeCurrent();

    const qreal retinaScale = m_renderWidget->devicePixelRatio();
    //glViewport(0, 0, m_renderWidget->width() * retinaScale, m_renderWidget->height() * retinaScale);
    m_camera->SetViewport(0,0, m_renderWidget->width() * retinaScale,  m_renderWidget->height() * retinaScale);

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.4,0.4,0.4,0);

    m_program->bind();

//    QMatrix4x4 projectionMatrix;
//    projectionMatrix.perspective(60.0f,  m_renderWidget->width()/m_renderWidget->height(), 0.1f, 100.0f);
//    m_program->setUniformValue(m_projectionMatrixU, projectionMatrix);

//    QMatrix4x4 viewMatrix;
//    viewMatrix.translate(0, 0, -10);
//    viewMatrix.rotate(100.0f * m_frame / QApplication::screens().at(0)->refreshRate(), 0, 1, 0);
//    m_program->setUniformValue(m_viewMatrixU, viewMatrix);


    // get Model and View from the camera
    glm::mat4 P;
    glm::mat4 V;
    glm::mat4 M;
    m_camera->Update();
    m_camera->GetMatricies(P,V,M);
    GLfloat* P_pointer;
    P_pointer = glm::value_ptr(P);
    GLfloat* V_pointer;
    V_pointer = glm::value_ptr(V);

    GLfloat (*P_array)[4] = (GLfloat (*)[4]) P_pointer;
    GLfloat (*V_array)[4] = (GLfloat (*)[4]) V_pointer;

    m_program->setUniformValue(m_projectionMatrixU, P_array);
    m_program->setUniformValue(m_viewMatrixU, V_array);

    QVector2D scale;
    scale.setX(1);
    scale.setY(1);
    m_program->setUniformValue(m_scaleAttr, scale);

    // update Data only if necessary
    if (m_currentFrame != m_lastFrame)
    {
        this->updateDataFrame();
        m_lastFrame = m_currentFrame;
    }

    glEnableVertexAttribArray(m_positionAttr);
    glEnableVertexAttribArray(m_colorAttr);
    glEnableVertexAttribArray(m_instancePositionAttr);

    gl45func->glVertexAttribDivisor(m_positionAttr,0);
    gl45func->glVertexAttribDivisor(m_colorAttr,1);
    gl45func->glVertexAttribDivisor(m_instancePositionAttr,1);

    gl45func->glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, m_numAtoms[m_currentFrame]);

    GLuint atomBuff;
    glGenBuffers(1, &atomBuff);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomBuff);
    gl45func->glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 3, atomBuff);
    gl45func->glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0);

    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    m_program->release();

    ++m_frame;
}
