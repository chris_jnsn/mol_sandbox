#ifndef DynamicMoleculeRenderer_DetectVisible_H
#define DynamicMoleculeRenderer_DetectVisible_H

#include "Meta/QRenderer.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include <QOpenGLShaderProgram>

class DynamicMoleculeRenderer_DetectVisible: public QRenderer
{
public:
    void render() Q_DECL_OVERRIDE;
    bool initializeOverride(Drawable *drawable);
    DynamicMoleculeRenderer_DetectVisible();

private:
    DynamicMolecule* m_dynamicMolecule;

    GLuint m_scaleAttr;
    GLuint m_viewMatrixU;
    GLuint m_projectionMatrixU;
    GLuint m_positionAttr;
    GLuint m_instancePositionAttr;
    GLuint m_colorAttr;
    GLuint m_drawCounterSSBO;

    QOpenGLShaderProgram *m_program;
    int m_frame;

    std::vector<std::vector<GLfloat> > m_instance_colors;
    std::vector<std::vector<GLfloat> > m_instance_positions;
    std::vector<GLfloat> m_uniformQuad;
    std::vector<int> m_numAtoms;

    void updateDataFrame();
    int m_lastFrame;
    int m_currentFrame;


};

#endif //DynamicMoleculeRenderer_DetectVisible_H
