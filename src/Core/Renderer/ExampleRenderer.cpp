//#include <GL/glew.h>
//#include <GLFW/glfw3.h>
#include "ExampleRenderer.h"
#include <iostream>
#include <QScreen>
#include <qapplication.h>
#include "Meta/qgltogl.h" //include this to macros to call QOpenGLFunctions


static const char *vertexShaderSource =
    "attribute highp vec4 posAttr;\n"
    "attribute lowp vec4 colAttr;\n"        
    "attribute vec4 offAttr;\n"
    "varying lowp vec4 col;\n"
    "uniform highp mat4 matrix;\n"
    "void main() {\n"
    "   col = colAttr;\n"
    "   gl_Position = matrix * posAttr + offAttr;\n"
    "}\n";

static const char *fragmentShaderSource =
    "varying lowp vec4 col;\n"
    "void main() {\n"
    "   gl_FragColor = col;\n"
    "}\n";

ExampleRenderer::ExampleRenderer()
    : m_program(0)
    , m_frame(0)
{

}

bool ExampleRenderer::initializeOverride(Drawable *drawable)
{
    m_dynamicMolecule = dynamic_cast<DynamicMolecule*>(drawable);

    m_program = new QOpenGLShaderProgram(m_renderWidget);
    m_program->addShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->link();
    m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_offAttr = m_program->attributeLocation("offAttr");
    m_matrixUniform = m_program->uniformLocation("matrix");
}


void ExampleRenderer::render()
{
    const qreal retinaScale = m_renderWidget->devicePixelRatio();
    glViewport(0, 0, m_renderWidget->width() * retinaScale, m_renderWidget->height() * retinaScale);

    glClear(GL_COLOR_BUFFER_BIT);

    m_program->bind();

    QMatrix4x4 matrix;
    matrix.perspective(60.0f, 4.0f/3.0f, 0.1f, 100.0f);
    matrix.translate(0, 0, -2);
    matrix.rotate(100.0f * m_frame / QApplication::screens().at(0)->refreshRate(), 0, 1, 0);

    m_program->setUniformValue(m_matrixUniform, matrix);

    GLfloat offset[] = {
        -0.5f, 0, 0,
        0.5f, 0, 0
    };

    GLfloat vertices[] = {
        0.0f, 0.707f,
        -0.5f, -0.5f,
        0.5f, -0.5f
    };

    GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
    };

    glVertexAttribPointer(m_posAttr, 2, GL_FLOAT, GL_FALSE, 0, vertices);
    glVertexAttribPointer(m_colAttr, 3, GL_FLOAT, GL_FALSE, 0, colors);
    glVertexAttribPointer(m_offAttr, 3, GL_FLOAT, GL_FALSE, 0, offset);

    glEnableVertexAttribArray(m_posAttr);
    glEnableVertexAttribArray(m_colAttr);
    glEnableVertexAttribArray(m_offAttr);

    glVertexAttribDivisor(m_offAttr,1);

    glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 3, 2);

    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(0);

    m_program->release();

    ++m_frame;

}
