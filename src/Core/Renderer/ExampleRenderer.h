#ifndef EXAMPLERENDERER_H
#define EXAMPLERENDERER_H

#include "Meta/QRenderer.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include <QOpenGLShaderProgram>

class ExampleRenderer: public QRenderer
{
public:
    void render() Q_DECL_OVERRIDE;
    bool initializeOverride(Drawable *drawable);
    ExampleRenderer();

private:
    DynamicMolecule* m_dynamicMolecule;

    GLuint m_posAttr;
    GLuint m_colAttr;
    GLuint m_offAttr;
    GLuint m_matrixUniform;

    QOpenGLShaderProgram *m_program;
    int m_frame;
    QOpenGLContext *m_context;
};

#endif //EXAMPLERENDERER_H
