#include "mainwindow.h"
#include <QLabel>
#include <QSurfaceFormat>
#include <QGridLayout>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QDockWidget>
#include <QSizePolicy>

#include "Reader/MDTrajh5Reader/MDTrajh5Reader.h"
#include "Renderer/DynamicMoleculeRenderer.h"
#include "QOpenGL/QOpenGLWidget/MainOpenGLWidget.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include "Widgets/FrameNavigator.h"
#include "Widgets/Loader.h"
#include "Meta/DataManager.h"
#include "Meta/DataNode.h"


#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QSurfaceFormat format;
    format.setSamples(16);
    format.setVersion(4,3);

    //QGridLayout *renderWindowContainer = findChild<QGridLayout*>("renderWindowContainer");
    //QOpenGLWidget *openGLWidget = findChild<QOpenGLWidget*>("openGLWidget");

    MainOpenGLWidget* mainOpenglwidget = new MainOpenGLWidget;
    mainOpenglwidget->setFormat(format);
    mainOpenglwidget->resize(640, 480);
    mainOpenglwidget->setAnimating(true);

    // Loading data ect. from here, due to lack of implemented widgets

    // load default model for development...
//    MDTrajh5Reader reader;
//    DynamicMolecule *mol = reader.loadDynamicMolecule("TrajectoryFiles/subset_nowater.h5");

//    // register the model with the datamanager default node
//    DataManager::getInstance().getDataNode()->addDataObject(mol);

//    DynamicMoleculeRenderer* renderer = new DynamicMoleculeRenderer;

//    mainOpenglwidget->addDrawable(mol, renderer);

    QMdiArea *mdiArea = new QMdiArea;
    mdiArea->tileSubWindows();
    QMdiSubWindow *subwindow_renderer = new QMdiSubWindow;
    subwindow_renderer->setWindowTitle(QString("3D View"));
    subwindow_renderer->setWidget(mainOpenglwidget);
    subwindow_renderer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    mdiArea->addSubWindow(subwindow_renderer, Qt::FramelessWindowHint);
    setCentralWidget(mdiArea);
    subwindow_renderer->showMaximized();

    QDockWidget *dock = new QDockWidget(tr("Frame Navigator"), this);
    FrameNavigator *frameNav = new FrameNavigator();
    dock->setWidget(frameNav);

    QDockWidget *dock2 = new QDockWidget(tr("File Loader"), this);
    Loader *loader = new Loader();
    dock2->setWidget(loader);

    addDockWidget(Qt::LeftDockWidgetArea, dock);
    addDockWidget(Qt::RightDockWidgetArea, dock2);
}

MainWindow::~MainWindow()
{
    delete ui;
}
