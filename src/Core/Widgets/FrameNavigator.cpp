#include "FrameNavigator.h"
#include <QGridLayout>

FrameNavigator::FrameNavigator(QWidget *parent)
    : QWidget(parent)
{
    this->create(tr("FrameNavigator"));
    // get the active data object
    this->activeDataObjectChanged();
}

FrameNavigator::~FrameNavigator()
{

}

void FrameNavigator::updateActiveFrameDataObject(int value)
{
    if (m_activeDataObject != NULL)
        m_activeDataObject->setCurrentFrame(value);
    else
    {
        this->activeDataObjectChanged(); // try to fetch the reference and try again
        if (m_activeDataObject != NULL)
            m_activeDataObject->setCurrentFrame(value);
    }
}

void FrameNavigator::create(const QString &title)
{
    //    minimumLabel = new QLabel(tr("Start Frame"));
    //    maximumLabel = new QLabel(tr("End Frame"));
    valueLabel = new QLabel(tr("Current Frame"));

    valueSpinBox = new QSpinBox;
    valueSpinBox->setRange(0,100);
    valueSpinBox->setSingleStep(1);

    slider = new QSlider(Qt::Horizontal, this);

    connect(valueSpinBox, SIGNAL(valueChanged(int)),
            slider, SLOT(setValue(int)));
    connect(slider, SIGNAL(valueChanged(int)),
            valueSpinBox, SLOT(setValue(int)));
    connect(slider, SIGNAL(valueChanged(int)),
            this, SLOT(updateActiveFrameDataObject(int)));

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(slider, 0, 0);
    layout->addWidget(valueLabel, 1, 0);
    layout->addWidget(valueSpinBox,1,1);
    setLayout(layout);

    setWindowTitle(title);
}

void FrameNavigator::activeDataObjectChanged()
{
    // A TimeResolvedDataObject is expected here:
    DataObject* o = DataManager::getInstance().getActiveDataObject();
    if (o == NULL) // no object available
        return;
    if (dynamic_cast<TimeResolvedDataObject*>(o) == NULL)
    {
        //active data object can not be handled by FrameNavigator
        // deactivate ...
    }
    else
    {
        // set properties ...
        m_activeDataObject = dynamic_cast<TimeResolvedDataObject*>(o);
        valueSpinBox->setRange(m_activeDataObject->minFrame(), m_activeDataObject->maxFrame());
        slider->setRange(m_activeDataObject->minFrame(), m_activeDataObject->maxFrame());
    }
}

void FrameNavigator::activeDataNodeChanged()
{

}
