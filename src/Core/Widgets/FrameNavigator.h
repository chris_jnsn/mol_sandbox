#ifndef FRAMENAVIGATOR_H
#define FRAMENAVIGATOR_H

#include <QGroupBox>
#include <QSlider>
#include <QLabel>
#include <QSpinBox>

#include "DataObjects/TimeResolvedDataObject.h"
#include "Meta/DataManager.h"
#include "Meta/DataManagerListener.h"


class FrameNavigator : public QWidget, public  DataManagerListener
{
    Q_OBJECT

public:
    FrameNavigator(QWidget *parent = 0);
    ~FrameNavigator();

    TimeResolvedDataObject *m_activeDataObject;

public slots:
    void updateActiveFrameDataObject(int value);

private:
    QLabel *valueLabel;
    QSlider *slider;
    QSpinBox *valueSpinBox;

    void create(const QString &title);



    // DataManagerListener interface
public:
    void activeDataObjectChanged();
    void activeDataNodeChanged();
};

#endif //FRAMENAVIGATOR_H
