#include "Loader.h"
#include <QGridLayout>
#include <QLineEdit>
#include <QFileDialog>

#include "Reader/MDTrajh5Reader/MDTrajh5Reader.h"

Loader::Loader(QWidget *parent)
    : QWidget(parent)
{
    this->create(tr("Loader"));
}

Loader::~Loader()
{

}

void Loader::browsePathButtonPressed()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open Mol Data"),m_pathEdit->text(), tr("Mol Files (*.h5 *.pdb)"), 0
                                                    , QFileDialog::DontUseNativeDialog); // workaround for a crashing dialog untre ubuntu 14.04 or whatever other reason

    if (fileName.isEmpty())
        return;

    m_pathEdit->setText(fileName);

    MDTrajh5Reader reader;
    DynamicMolecule *mol = reader.loadDynamicMolecule(fileName.toStdString());
    DataManager::getInstance().getDataNode()->addDataObject(mol);
}

void Loader::create(const QString &title)
{

    m_pathEdit = new QLineEdit;
    m_pathEdit->setText("TrajectoryFiles/subset_nowater.h5"); //set a defaul location
    m_browsePathButton = new QPushButton;

    m_browsePathButton->setText("Load File");

    QGridLayout *layout = new QGridLayout;
    layout->addWidget(m_pathEdit, 0, 0);
    layout->addWidget(m_browsePathButton, 0, 1);
    setLayout(layout);

    connect(m_browsePathButton, SIGNAL (pressed()), this, SLOT (browsePathButtonPressed()));

    setWindowTitle(title);
}

void Loader::activeDataObjectChanged()
{

}

void Loader::activeDataNodeChanged()
{

}
