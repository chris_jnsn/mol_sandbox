#ifndef LOADER_H
#define LOADER_H

#include <QGroupBox>
#include <QSlider>
#include <QLabel>
#include <QSpinBox>
#include <QPushButton>

#include "DataObjects/TimeResolvedDataObject.h"
#include "Meta/DataManager.h"
#include "Meta/DataManagerListener.h"


class Loader : public QWidget, public  DataManagerListener
{
    Q_OBJECT

public:
    Loader(QWidget *parent = 0);
    ~Loader();

public slots:
    void browsePathButtonPressed();

private:

    void create(const QString &title);
    QLineEdit *m_pathEdit;
    QPushButton *m_browsePathButton;

    // DataManagerListener interface
public:
    void activeDataObjectChanged();
    void activeDataNodeChanged();
};

#endif //LOADER_H
