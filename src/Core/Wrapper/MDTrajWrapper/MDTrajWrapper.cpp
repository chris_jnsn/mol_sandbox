#include "MDTrajWrapper.h"
#include <iostream>
#include <numpy/arrayobject.h>
#include <Python.h>


MDTrajWrapper::MDTrajWrapper()
{
    Py_Initialize();
    PyObject *sys = PyImport_ImportModule("sys");
    PyObject *path = PyObject_GetAttrString(sys, "path");
    std::string s = MDTRAJ_PATH;
    PyList_Append(path, PyUnicode_FromString(s.c_str())); // add location of the MDTraj package to python path

    this->importMDTraj();
}

void MDTrajWrapper::importMDTraj()
{
    PyObject* MDTrajString = PyUnicode_FromString((char*)"mdtraj");
    PyObject* MDTraj = PyImport_Import(MDTrajString);
    if (MDTraj == nullptr)
    {
        std::cerr << "Python Error. Printing Python Stacktrace..." << std::endl;
        PyErr_Print();
        std::exit(1);
    }

    // get access to functions
    function_load = PyObject_GetAttrString(MDTraj, (char*)"load");
}

PyObject* MDTrajWrapper::loadFile(std::string path)
{
    PyObject* args = PyTuple_Pack(1, PyUnicode_FromString(path.c_str()));
    PyObject* file = PyObject_CallObject(function_load, args);

    //    PyObject* top = PyObject_GetAttrString(file, "topology");
    //    PyObject* atom_list = PyObject_GetAttrString(top, "atom");
    //    long i = 3;
    //    PyObject* arg = PyTuple_Pack(1, PyLong_FromLong(i));
    //    PyObject* atom = PyObject_CallObject(atom_list, arg);
    //    PyObject* pyindex =  PyObject_GetAttrString(atom, "index");
    //    int index = PyInt_AsLong(pyindex);

    return file;
}

PyObject *MDTrajWrapper::getXYZ(PyObject *file)
{
    return PyObject_GetAttrString(file, (char*)"xyz");
}

PyObject *MDTrajWrapper::getTopology(PyObject *file)
{
    return PyObject_GetAttrString(file, (char*)"topology");
}

void MDTrajWrapper::getAllAtomProperties(PyObject *topology, std::vector<std::string> &names,
                                         std::vector<std::string> &elementNames, std::vector<std::string> &residueNames,
                                         std::vector<int> &indices, std::vector<int> &numBonds)
{
    PyObject* n_atoms_py = PyObject_GetAttrString(topology, "n_atoms");
    int n_atoms = PyLong_AsLong(n_atoms_py);

    PyObject* atom_list = PyObject_GetAttrString(topology, "atom");

    PyObject* arg;
    PyObject* atom;
    PyObject* name_py;
    PyObject* index_py;
    PyObject* bonds_py;

    PyObject* element_py;
    PyObject* element_name_py;

    PyObject* residue_py;
    PyObject* residue_name_py;

    for(int i = 0; i < n_atoms; i++)
    {
        arg = PyTuple_Pack(1, PyLong_FromLong(i));
        atom = PyObject_CallObject(atom_list, arg);
        name_py =  PyObject_GetAttrString(atom, "name");
        index_py =  PyObject_GetAttrString(atom, "index");
        bonds_py =  PyObject_GetAttrString(atom, "n_bonds");

        element_py =  PyObject_GetAttrString(atom, "element");
        element_name_py =  PyObject_GetAttrString(element_py, "name");

        residue_py =  PyObject_GetAttrString(atom, "residue");
        residue_name_py =  PyObject_GetAttrString(residue_py, "name");

        names.push_back(PyUnicode_AsUTF8(name_py));
        elementNames.push_back(PyUnicode_AsUTF8(element_name_py));
        residueNames.push_back(PyUnicode_AsUTF8(residue_name_py));
        indices.push_back(PyLong_AsLong(index_py));
        numBonds.push_back(PyLong_AsLong(bonds_py));
    }
}

MDTrajWrapper::~MDTrajWrapper()
{
    Py_Finalize();
}
