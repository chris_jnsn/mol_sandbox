#ifndef MDTRAJWRAPPER_H
#define MDTRAJWRAPPER_H

#include <string>
#include <vector>

#include <Python.h>



class MDTrajWrapper{

public:


    MDTrajWrapper();
    ~MDTrajWrapper();

    PyObject* loadFile(std::string path);

    /**
     * @brief extractXYZ
     * @param file
     * @return Returns the full trajectory of the given file
     */
    PyObject* getXYZ(PyObject* file);
    PyObject* getTopology(PyObject* file);

    /**
     * @brief getAllAtomNames
     * @param topology
     * @return Returns all atom names ordered by atom indeces 0..n-1
     */
    void getAllAtomProperties(PyObject* topology, std::vector<std::string> &names, std::vector<std::string> &elementNames
                              , std::vector<std::string> &residueNames, std::vector<int> &indices
                              , std::vector<int> &numBonds);
    void importMDTraj();



private:
    PyObject* function_load;

};
#endif //MDTRAJWRAPPER_H
