#include <Python.h> // include python before Qt, since Qt defines "slot" as a keyword
                    // and python uses a variable named "slot"
#undef B0           // this is defined by python, but Qt wants to use this as well

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <QApplication>
#include <QGLFormat>
#include <QWidget>
#include <QGridLayout>
#include <QHBoxLayout>

//#include "Widgets/QOpenGLWidget/GLWidget.h"
#include "Wrapper/MDTrajWrapper/MDTrajWrapper.h"
#include "Reader/MDTrajh5Reader/MDTrajh5Reader.h"
#include "DrawableDataObjects/DynamicMoleculeDrawable/DynamicMolecule.h"
#include "Renderer/DynamicMoleculeRenderer.h"
#include "Renderer/ExampleRenderer.h"
#include "UI/mainwindow.h"
#include "QOpenGL/QOpenGLWidget/MainOpenGLWidget.h"





int main( int argc, char* argv[] )
{
    glfwInit();
    //QGuiApplication app(argc, argv);
    QApplication app(argc, argv);

  //  QSurfaceFormat format;
  //  format.setSamples(16);
  //  format.setVersion(4,3);

    MainWindow mainWindow;
    //QWidget* windowContainer = QWidget::createWindowContainer(&window);

    //mainWindow.setCentralWidget(windowContainer);
  //  MainOpenGLWidget openglwidget;
 //   openglwidget.setFormat(format);
  //  openglwidget.resize(640, 480);
 //   openglwidget.setAnimating(true);

    mainWindow.show();


    // Python Test
    MDTrajh5Reader reader;

   // DynamicMolecule mol = reader.loadDynamicMolecule("TrajectoryFiles/traj.h5");
  //  DynamicMoleculeRenderer renderer;

    //openglwidget.addDrawable(&mol, &renderer);
  //  openglwidget.show();
  //  openglwidget.addDrawable(&mol, &renderer);

    return app.exec();
}
