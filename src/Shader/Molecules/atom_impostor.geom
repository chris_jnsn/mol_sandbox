#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 view;
uniform mat4 projection;

in float vs_atom_radius[];

flat out vec4  gs_atom_position_world;
flat out vec4  gs_atom_position_cam;
flat out float gs_atom_radius;
out vec2       gs_impostor_coord;


void draw_vertex(vec2 offset)
{
    gs_impostor_coord = offset;

    gl_Position     = gs_atom_position_cam;
    gl_Position.xy += gs_atom_radius * offset;
    gl_Position     = projection * gl_Position;

    EmitVertex();
}

void main()
{
    gs_atom_position_world = gl_in[0].gl_Position;
    gs_atom_position_cam   = view * gs_atom_position_world;

    gs_atom_radius         = vs_atom_radius[0];

    draw_vertex(vec2(-1.0, -1.0));
    draw_vertex(vec2(-1.0,  1.0));
    draw_vertex(vec2( 1.0, -1.0));
    draw_vertex(vec2( 1.0,  1.0));

    EndPrimitive();
}
