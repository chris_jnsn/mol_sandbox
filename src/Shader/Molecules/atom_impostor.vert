#version 450

in vec3  atom_position;
in float atom_radius;

out float vs_atom_radius;


void main()
{
    gl_Position = vec4(atom_position, 1.0);
    vs_atom_radius = atom_radius;
}
