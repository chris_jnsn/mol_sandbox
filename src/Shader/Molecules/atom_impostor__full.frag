#version 450

uniform mat4 view;
uniform mat4 projection;

flat in vec4  gs_atom_position_world;
flat in vec4  gs_atom_position_cam;
flat in float gs_atom_radius;
in vec2       gs_impostor_coord;

out vec4 frag_color;
layout (depth_less) out float gl_FragDepth;

const vec3 light_direction = normalize(vec3(1.0, -2.0, 1.0));
const vec4 light_color     = vec4(1.0, 0.8, 0.8, 1.0);
const vec4 ambient_color   = vec4(0.8, 0.8, 1.0, 1.0);


void main()
{
    float x = gs_impostor_coord.x;
    float y = gs_impostor_coord.y;
    float zz = 1.0 - x*x - y*y;

    if (zz <= 0.0)
    {
        discard;
    }

    float z = sqrt(zz);

    vec4 hit_position_cam  = gs_atom_position_cam;
    hit_position_cam.x    += x * gs_atom_radius;
    hit_position_cam.y    += y * gs_atom_radius;
    hit_position_cam.z    += z * gs_atom_radius;

    vec4 hit_position_screen = projection * hit_position_cam;

    gl_FragDepth = 0.5 * (hit_position_screen.z / hit_position_screen.w) + 0.5;
    // The largest issue with performance is that writing to gl_FragDepth prevents early-z culling, so drawing lots of geometry will be slow due to overdraw!
    // It is best to use in combination with a rendering pass that buckets a few layers of close geometry to reduce overdraw.


    vec3 normal = vec3(x, y, z);


    vec3 reflect_direction  = reflect(light_direction, normal);
    float reflectance       = pow(dot(light_direction, reflect_direction) * 0.5 + 0.5, 2.0);
    float diffuse           = max(0.0, dot(light_direction, normal) * 0.5 + 0.5);

    frag_color = ambient_color * 0.2 +  //ambient
                 light_color * diffuse * 0.7 +  //diffuse
                 vec4(0.1f, 0.1f, 0.3f, 1.0f) * reflectance * 0.7; //reflection

}
