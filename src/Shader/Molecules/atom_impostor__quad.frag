#version 450

flat in vec4  gs_atom_position_world;

out vec4 frag_color;


void main()
{
    frag_color = vec4(normalize(abs(gs_atom_position_world.xyz)), 1.0);
}
