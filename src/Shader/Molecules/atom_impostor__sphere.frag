#version 450

uniform mat4 view;
uniform mat4 projection;

flat in vec4  gs_atom_position_world;
flat in vec4  gs_atom_position_cam;
flat in float gs_atom_radius;
in vec2       gs_impostor_coord;

out vec4 frag_color;
layout (depth_less) out float gl_FragDepth;


void main()
{
    float x = gs_impostor_coord.x;
    float y = gs_impostor_coord.y;
    float zz = 1.0 - x*x - y*y;

    if (zz <= 0.0)
    {
        discard;
    }

    float z = sqrt(zz);

    vec4 pos     = gs_atom_position_cam;
    pos.z       += gs_atom_radius * z;
    pos          = projection * pos;
    gl_FragDepth = 0.5 * (pos.z / pos.w) + 0.5;


    frag_color = vec4(normalize(abs(gs_atom_position_world.xyz)), 1.0);
}
