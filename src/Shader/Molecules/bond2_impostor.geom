#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 view;
uniform mat4 projection;

in float vs_atom_radius[];

flat out vec4  gs_atom_position_world;
flat out vec4  gs_atom_position_cam;
flat out float gs_atom_radius;
out vec2       gs_impostor_coord;


void draw_vertex(vec2 offset)
{
    gs_impostor_coord = offset;

    gl_Position     = gs_atom_position_cam;
    gl_Position.xy += gs_atom_radius * offset;
    gl_Position     = projection * gl_Position;

    EmitVertex();
}

void main()
{
    gs_atom_position_world = gl_in[0].gl_Position;
    gs_atom_position_cam   = view * gs_atom_position_world;
    gs_atom_radius         = vs_atom_radius[0];

    draw_vertex(vec2(-1.0, -1.0));
    draw_vertex(vec2(-1.0,  1.0));
    draw_vertex(vec2( 1.0, -1.0));
    draw_vertex(vec2( 1.0,  1.0));

    EndPrimitive();
}



#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 5) out;

uniform mat4 projection;

in vec4 vs_atom1_position[];
in vec4 vs_atom2_position[];
in vec4 vs_atom3_position[];

flat out vec4 gs_probe_position;
flat out vec4 gs_atom1_position;
flat out vec4 gs_atom2_position;
flat out vec4 gs_atom3_position;

out vec3 pos;

void draw_vertex(vec4 position)
{
    pos = position.xyz;
    gl_Position = projection * position;

    EmitVertex();
}

void main()
{
    vec4 probe_position = gl_in[0].gl_Position;

    gs_probe_position = probe_position;
    gs_atom1_position = vs_atom1_position[0];
    gs_atom2_position = vs_atom2_position[0];
    gs_atom3_position = vs_atom3_position[0];

    draw_vertex(vs_atom1_position[0]);
    draw_vertex(vs_atom2_position[0]);
    draw_vertex(probe_position);
    draw_vertex(vs_atom3_position[0]);
    draw_vertex(vs_atom1_position[0]);

    EndPrimitive();
}
