#version 450

in vec3  atom_position;
in float atom_radius;

out float vs_atom_radius;


void main()
{
    gl_Position = vec4(atom_position, 1.0);
    vs_atom_radius = atom_radius;
}


#version 450

uniform mat4 model;
uniform mat4 view;

in vec3 probe_position;

in vec3 atom1_position;
in vec3 atom2_position;
in vec3 atom3_position;

out vec4 vs_atom1_position;
out vec4 vs_atom2_position;
out vec4 vs_atom3_position;


void main()
{
    vs_atom1_position = view * model * vec4(atom1_position, 1.0);
    vs_atom2_position = view * model * vec4(atom2_position, 1.0);
    vs_atom3_position = view * model * vec4(atom3_position, 1.0);

    gl_Position       = view * model * vec4(probe_position, 1.0);
}
