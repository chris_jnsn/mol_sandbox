#version 450

uniform mat4 view;

flat in vec4  gs_atom_position_world;
flat in vec4  gs_atom_position_cam;
flat in float gs_atom_radius;
in vec2       gs_impostor_coord;

out vec4 frag_color;


void main()
{
    frag_color = vec4(normalize(abs(gs_atom_position_world.xyz)), 1.0);
}






#version 450

#define occlusion_enabled
#define occlusion_quality 4
//#define occlusion_preview

const int   raymarch_steps_max = 128;
const float raymarch_epsilon   = 0.001;

uniform mat4  view;
uniform vec3  light_direction;
uniform vec4  light_color;
uniform float light_power;
uniform vec4  ambient_color;
uniform float probe_radius;

flat in vec4 gs_probe_position;
flat in vec4 gs_atom1_position;
flat in vec4 gs_atom2_position;
flat in vec4 gs_atom3_position;

in vec4 pos;

out vec4 frag_color;


/* Checks whether the specified point is inside the tetrahedron (by index)
*  based on this approach: http://steve.hollasch.net/cgindex/geometry/ptintet.html
*  @return true if the point_ is inside the tetrahedron (or on one of the four triangles), otherwise false
*  @param point_ the Vec3f point to be checked
*  @param tetra_ the tetrahedron
*/
bool check_position_in_tetrahedron(vec3 position)
{
    vec4 v0 = vec4((inverse(view) * gs_probe_position).xyz, 1.0);
    vec4 v1 = vec4((inverse(view) * gs_atom1_position).xyz, 1.0);
    vec4 v2 = vec4((inverse(view) * gs_atom2_position).xyz, 1.0);
    vec4 v3 = vec4((inverse(view) * gs_atom3_position).xyz, 1.0);

    vec4 p0 = vec4(position, 1.0);

    float det0 = determinant(mat4(v0, v1, v2, v3));
    float det1 = determinant(mat4(p0, v1, v2, v3));
    float det2 = determinant(mat4(v0, p0, v2, v3));
    float det3 = determinant(mat4(v0, v1, p0, v3));
    float det4 = determinant(mat4(v0, v1, v2, p0));

    /**
    If by chance the Determinant det0 is 0, then your tetrahedron is degenerate (the points are coplanar).
    If any other Di=0, then P lies on boundary i (boundary i being that boundary formed by the three points other than Vi).
    If the sign of any Di differs from that of D0 then P is outside boundary i.
    If the sign of any Di equals that of D0 then P is inside boundary i.
    If P is inside all 4 boundaries, then it is inside the tetrahedron.
    As a check, it must be that D0 = D1+D2+D3+D4.
    The pattern here should be clear; the computations can be extended to simplicies of any dimension. (The 2D and 3D case are the triangle and the tetrahedron).
    If it is meaningful to you, the quantities bi = Di/D0 are the usual barycentric coordinates.
    Comparing signs of Di and D0 is only a check that P and Vi are on the same side of boundary i.
    */
    if (det0 != 0)
    {
        if (det0 < 0)
        {
            if ((det1 < 0) && (det2 < 0) && (det3 < 0) && (det4 < 0))
            {
                return true;
            }
        }
        if (det0 > 0)
        {
            if ((det1 > 0) && (det2 > 0) && (det3 > 0) && (det4 > 0))
            {
                return true;
            }
        }
    }
    return false;
}

float distance_to_surface(vec3 position)
{
    return probe_radius - length(position - (inverse(view) * gs_probe_position).xyz);
}

void main()
{
    vec4 cam_pos = inverse(view) * vec4(0.0, 0.0, 0.0, 1.0);
    vec4 world_pos = inverse(view) * pos;

    vec3 ray_direction = normalize((world_pos - cam_pos).xyz);

    float distance = distance_to_surface(world_pos.xyz);

    if (distance < 0) // outside of probe
    {
        discard;
    }

    vec3 real_position = world_pos.xyz;
    float s = 0.0;

    for (int step = 0; step < raymarch_steps_max; step++)
    {
        s = step;
        real_position += ray_direction * distance;

        distance = distance_to_surface(real_position.xyz);

        if (abs(distance) < raymarch_epsilon)
        {
            break;
        }
        if (step == 2)
        {
            if (!check_position_in_tetrahedron(real_position.xyz)) // early break to prevent itereation over outside points
            {
                discard;
            }
        }
    }

    if (!check_position_in_tetrahedron(real_position.xyz))
    {
        discard;
    }


    vec3 normal = normalize((inverse(view) * gs_probe_position).xyz - real_position);
    vec3 reflect_direction = reflect(ray_direction, normal);

    float reflectance = pow(dot(ray_direction, reflect_direction) * 0.5 + 0.5, 2.0);
        float diffuse = max(0.0, dot(light_direction, normal) * 0.5 + 0.5);

    float oa = 1.0;
    float od = 1.0;
    float os = 1.0;

        #ifdef occlusion_preview
                frag_color = vec3((oa + od + os) * 0.3);
        #else
                frag_color =
            ambient_color * oa * 0.2 +  //ambient
                    light_color * diffuse * od * 0.7 +  //diffuse
                    vec4(0.1f, 0.1f, 0.3f, 1.0f) * os * reflectance * 0.7; //reflection
        #endif


    //frag_color = vec4(s/raymarch_steps_max,0.0,0.0,1.0);  // visualise steps
}


// wireframe
// http://codeflow.org/entries/2012/aug/02/easy-wireframe-display-with-barycentric-coordinates/


// light_color
// https://github.com/opengl-tutorials/ogl/blob/master/tutorial08_basic_shading/StandardShading.fragmentshader

// http://steve.hollasch.net/cgindex/geometry/ptintet.html

// https://www.shadertoy.com/view/Mss3WN
// http://www.lighthouse3d.com/tutorials/glsl-12-tutorial/the-normal-matrix/
// Uniform values are constants, they don't change between vertices
// varying variables, which change for each fragment,
// attribute - for each vertex

// http://dennis2society.de/main/painless-tetrahedral-barycentric-mapping

/*
> > Given a point A(x,y,z) and a tetrahedron.
> > How to determine A is inside or outside the tetrahedron?
> >
> > Thanks.
> > Michael.

1)  For each face, determine the center of the plane (average the corner
coordinates)

2)  Determine the normal of the plane (use cross products)

3)  Define a vector from the plane center to the test point.

4)  Take the dot product of the normal and the above vector.

If all four dot products are negative, then the point is inside.  If any
are positive, then the point is outside.  If any are zero, the point
lies on one of the tetrahedral planes.

*/

// In camera space, the camera is at the origin (0,0,0).

