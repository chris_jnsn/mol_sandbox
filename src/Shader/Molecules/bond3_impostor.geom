#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 5) out;

uniform mat4 view;
uniform mat4 projection;
uniform float probe_radius;

in vec3  vs_atom1_position[];
in float vs_atom1_radius[];
in vec3  vs_atom2_position[];
in float vs_atom2_radius[];
in vec3  vs_atom3_position[];
in float vs_atom3_radius[];

flat out vec4 gs_probe_position_world;
flat out vec4 gs_probe_position;
flat out vec4 gs_atom1_position;
flat out vec4 gs_atom2_position;
flat out vec4 gs_atom3_position;

out vec3 gs_hit_position;

void draw_vertex(vec4 position)
{
    gs_hit_position = position.xyz;
    gl_Position = projection * position;

    EmitVertex();
}

void main()
{
    vec3 vector_atom1_atom2 = vs_atom2_position[0] - vs_atom1_position[0];
    float distance_atom1_atom2 = length(vector_atom1_atom2);
    float distance_atom1_intercection1 = (distance_atom1_atom2 * distance_atom1_atom2 -
                                          (vs_atom2_radius[0] + probe_radius) * (vs_atom2_radius[0] + probe_radius) +
                                          (vs_atom1_radius[0] + probe_radius) * (vs_atom1_radius[0] + probe_radius)) /
                                          (2 * distance_atom1_atom2);
    float radius_intercection1 = sqrt(abs((vs_atom1_radius[0] + probe_radius) * (vs_atom1_radius[0] + probe_radius) -
                                          distance_atom1_intercection1 * distance_atom1_intercection1));
    vec3 omega_x = vector_atom1_atom2 / distance_atom1_atom2;
    vec3 omega = vs_atom1_position[0] + distance_atom1_intercection1 * omega_x;
    vec3 vector_omega_atom3 = vs_atom3_position[0] - omega;
    float distance_omega_atom3 = length(vector_omega_atom3);
    vec3 omega_y = vector_omega_atom3 / distance_omega_atom3;
    vec3 omega_z = normalize(cross(omega_x, omega_y));
    float distance_omega_intercection2 = (distance_omega_atom3 * distance_omega_atom3 -
                                          (vs_atom3_radius[0] + probe_radius) * (vs_atom3_radius[0] + probe_radius) +
                                          radius_intercection1 * radius_intercection1) /
                                          (2 * distance_omega_atom3);
    float radius_intercection2 = sqrt(abs(radius_intercection1 * radius_intercection1 -
                                          distance_omega_intercection2 * distance_omega_intercection2));
    mat3 matrix_omega_axis = transpose(mat3(omega_x, omega_y, omega_z));
    vec3 probe_position1 = omega + vec3(0, distance_omega_intercection2,  radius_intercection2) * matrix_omega_axis;
    vec3 probe_position2 = omega + vec3(0, distance_omega_intercection2, -radius_intercection2) * matrix_omega_axis;
    vec3 center_position = omega + vec3(0, distance_omega_intercection2, 0) * matrix_omega_axis;
    vec3 vector_center_probe1 = vec3(0, 0, radius_intercection2) * matrix_omega_axis;

    gs_atom1_position = view * vec4(vs_atom1_position[0], 1.0);
    gs_atom2_position = view * vec4(vs_atom2_position[0], 1.0);
    gs_atom3_position = view * vec4(vs_atom3_position[0], 1.0);

    if (dot(-(view * vec4(center_position, 1.0)), view * vec4(vector_center_probe1, 0.0)) > 0.0) // < 90 degree --> looking towards cam
    {
        gs_probe_position_world = vec4(probe_position1, 1.0);
        gs_probe_position = view * vec4(probe_position1, 1.0);
    }
    else
    {
        gs_probe_position_world = vec4(probe_position2, 1.0);
        gs_probe_position = view * vec4(probe_position2, 1.0);
    }

    draw_vertex(gs_atom1_position);
    draw_vertex(gs_atom2_position);
    draw_vertex(gs_probe_position);
    draw_vertex(gs_atom3_position);
    draw_vertex(gs_atom1_position);

    EndPrimitive();
}
