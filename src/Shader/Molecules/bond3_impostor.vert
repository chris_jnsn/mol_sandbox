#version 450

in vec3  atom1_position;
in float atom1_radius;
in vec3  atom2_position;
in float atom2_radius;
in vec3  atom3_position;
in float atom3_radius;

out vec3  vs_atom1_position;
out float vs_atom1_radius;
out vec3  vs_atom2_position;
out float vs_atom2_radius;
out vec3  vs_atom3_position;
out float vs_atom3_radius;


void main()
{
    vs_atom1_position = atom1_position;
    vs_atom1_radius   = atom1_radius;
    vs_atom2_position = atom2_position;
    vs_atom2_radius   = atom2_radius;
    vs_atom3_position = atom3_position;
    vs_atom3_radius   = atom3_radius;

    gl_Position       = vec4((atom1_position + atom2_position + atom3_position) / 3, 1.0);
}
