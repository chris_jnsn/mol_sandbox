#version 450

#define occlusion_enabled
#define occlusion_quality 4
//#define occlusion_preview

const int   raymarch_steps_max = 256;
const float raymarch_epsilon   = 0.0001;

uniform mat4  view;
uniform mat4  projection;
uniform float probe_radius;

flat in vec4 gs_probe_position;
flat in vec4 gs_atom1_position;
flat in vec4 gs_atom2_position;
flat in vec4 gs_atom3_position;

in vec4 gs_hit_position;

out vec4 frag_color;
layout (depth_less) out float gl_FragDepth;


bool check_position_in_tetrahedron(vec3 position)
{
    vec4 v0 = vec4((inverse(view) * gs_probe_position).xyz, 1.0);
    vec4 v1 = vec4((inverse(view) * gs_atom1_position).xyz, 1.0);
    vec4 v2 = vec4((inverse(view) * gs_atom2_position).xyz, 1.0);
    vec4 v3 = vec4((inverse(view) * gs_atom3_position).xyz, 1.0);

    vec4 p0 = vec4(position, 1.0);

    float det0 = determinant(mat4(v0, v1, v2, v3));
    float det1 = determinant(mat4(p0, v1, v2, v3));
    float det2 = determinant(mat4(v0, p0, v2, v3));
    float det3 = determinant(mat4(v0, v1, p0, v3));
    float det4 = determinant(mat4(v0, v1, v2, p0));

    if (det0 != 0)
    {
        if (det0 < 0)
        {
            if ((det1 < 0) && (det2 < 0) && (det3 < 0) && (det4 < 0))
            {
                return true;
            }
        }
        if (det0 > 0)
        {
            if ((det1 > 0) && (det2 > 0) && (det3 > 0) && (det4 > 0))
            {
                return true;
            }
        }
    }
    return false;
}

float distance_to_surface(vec3 position)
{
    return probe_radius - length(position - (inverse(view) * gs_probe_position).xyz);
}

void main()
{
    vec4 hit_position_world = inverse(view) * gs_hit_position;
    vec4 cam_position_world = inverse(view) * vec4(0.0, 0.0, 0.0, 1.0);

    vec3 ray_direction = normalize((hit_position_world - cam_position_world).xyz);

    float distance = distance_to_surface(hit_position_world.xyz);

    if (distance < 0) // outside of probe
    {
        discard;
    }

    vec3 real_position_world = hit_position_world.xyz;
    float s = 0.0;

    for (int step = 0; step < raymarch_steps_max; step++)
    {
        s = step;
        real_position_world += ray_direction * distance;

        distance = distance_to_surface(real_position_world.xyz);

        if (abs(distance) < raymarch_epsilon)
        {
            break;
        }
        if (step == 2)
        {
            if (!check_position_in_tetrahedron(real_position_world.xyz)) // early break to prevent itereation over outside points
            {
                discard;
            }
        }
    }

    if (!check_position_in_tetrahedron(real_position_world.xyz))
    {
        discard;
    }

    frag_color = vec4(normalize(abs((inverse(view) * gs_probe_position).xyz)), 1.0);


    vec4 real_position_screen = projection * view * vec4(real_position_world, 1.0);

    gl_FragDepth = 0.5 * (real_position_screen.z / real_position_screen.w) + 0.5;
}
