#version 450

layout(points) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 view;
uniform mat4 projection;

in vec4  vs_atom1_position[];
in float vs_atom1_radius[];
in vec4  vs_atom2_position[];
in float vs_atom2_radius[];
in vec4  vs_atom3_position[];
in float vs_atom3_radius[];

flat out vec4  gs_center_position_world;


void draw_vertex(vec4 position)
{
    gl_Position = projection * view * position;

    EmitVertex();
}

void main()
{
    gs_center_position_world = gl_in[0].gl_Position;

    draw_vertex(vs_atom1_position[0]);
    draw_vertex(vs_atom2_position[0]);
    draw_vertex(vs_atom3_position[0]);

    EndPrimitive();
}
