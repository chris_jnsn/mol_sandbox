cmake_minimum_required(VERSION 2.8)
if(CMAKE_VERSION VERSION_GREATER "2.8.11")
    CMAKE_POLICY(SET CMP0022 OLD)
endif()

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
FIND_PACKAGE(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL)

if (NOT PYTHON_INCLUDE_DIRS)
    find_package(PythonLibs 3 REQUIRED)
endif()
find_package(NumPy REQUIRED)


include_directories(
    ${CORE_PATH}
${PYTHON_INCLUDE_DIRS}
${NUMPY_INCLUDE_DIRS}
)

file(GLOB_RECURSE SOURCES *.cpp)
file(GLOB_RECURSE HEADER *.h)

add_executable(${ProjectId} ${SOURCES} ${HEADER})

find_package(X11)

target_link_libraries(
    ${ProjectId}
    ${Qt5Widgets_LIBRARIES}
    ${Qt5OpenGL_LIBRARIES}
    ${PYTHON_LIBRARIES}
    ${ALL_DEPENDENCIES}
    ${X11_LIBRARIES}
    Xcursor Xinerama Xrandr Xxf86vm Xi pthread
)
