cmake_minimum_required(VERSION 2.8)
if(CMAKE_VERSION VERSION_GREATER "2.8.11")
    CMAKE_POLICY(SET CMP0022 OLD)
endif()

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})

FIND_PACKAGE(Qt5Widgets REQUIRED)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
include_directories(
    ${CORE_PATH}
)

#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")

file(GLOB_RECURSE SOURCES *.cpp)
file(GLOB_RECURSE HEADER *.h)
file(GLOB_RECURSE UI *.ui)

QT5_WRAP_UI(UIS_HDRS ${UI})

# to include ui_*.h files
include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${ProjectId} ${SOURCES} ${HEADER} ${UIS_HDRS})

target_link_libraries(
    ${ProjectId}
    ${ALL_DEPENDENCIES}
)
